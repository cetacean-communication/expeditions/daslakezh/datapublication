# DASLakeZH

This git repository  
[https://gitlab.switch.ch/cetacean-communication/expeditions/daslakezh/datapublication.git](https://gitlab.switch.ch/cetacean-communication/expeditions/daslakezh/datapublication.git) 

is accompanying the data publication  

**Test experiments with fiber-optic distributed acoustic sensing and hydrophone arrays for locaating underwater sounds.**  

Data:   
[DOI: 10.5281/zenodo.7657352](https://doi.org/10.5281/zenodo.7886409)

Report:    
[DOI: 10.48550/arXiv.2306.04276](https://doi.org/10.48550/arXiv.2306.04276)

please cite as:
Rychen, J., Paitz, P., Edme, P., Smolinski, K., Brackenhoff, J., & Fichtner, A. (2023). Test experiments with distributed acoustic sensing and hydrophone arrays for locating underwater sound sources. https://doi.org/10.5281/zenodo.7886409
 
## Description
The consolidation and compilation of the published datasets comprised these processing steps:

* Find the onset of each test signal playback and cut out a time segment of 25 seconds from the DAS recordings and the hydrophone recordings. Combine all signals into one HDF5 file.  
* Apply the calibration of the hydrophones and the speaker. This yields a quantitative measurement of sound pressure in the units of Pa.
* Project the geographic information into local cartesian coordinate system. Read in all GPS tracks and provide an estimate of the fiber location. Write all tracks and the bathymetry data of the lake into the dedicated file ‘Sitation.h5’  
* Add all needed and known metadata to each file.  


## Installation
JupyterLab with Python 3.10.   
See the file DASLakeZH_env.yml for a list of packages that are needed.   
Run the following conda commands to create the environment:  
conda env create -f DASLakeZH_env.yml
conda activate DASLake_env

## Usage
The notebooks in the folder "Compile" are for documenting the consolidation and compilation process of the published datasets. These routines read from a folder "RawData" and write to the folder "PubData". Snce the "RawData" is not published you can not reproduce the published data. However, should that become necessary, we are keeping a safe copy of the "RawData". 

The notebooks under the folder "Explore" are examples to read and visualize the data. All figures in the data description article can be reproduced with this scipts. 

the notebook under the folder "SpeakerCalibration" is from an other experiment were we measured the frequency dependent gain of the eletroacoustic transducer used in this experiment as a source. 

## Author 
Jörg Rychen  
jrychen@ethz.ch  
Institute of Neuroinformatics  
University of Zurich and ETH Zurich  
Winterthurerstrasse 190  
CH-8057 Zurich  
Switzerland  

## Acknowledgment
Thank you Patrick Paitz for your wonderful Jupyter notebooks that were my starting point to learn Python. 

## License
GNU 3.0

## Project status
in development
